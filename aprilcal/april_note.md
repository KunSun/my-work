# [张正友标定法](https://yq.aliyun.com/articles/62472)与[绝对二次曲线](https://blog.csdn.net/YhL_Leo/article/details/49357087)

张正友标定法的本质还是辨认出虚圆点。具体是通过固体标定平面将空间坐标降为2维，这样摄影变换就是2d-2d的单应变换,它有8个自由度，需要>=4组对应点来确定。每一对单应变换确定两个约束方程，即IAC上的两个点。三组单应变换就可以确定6个方程，求解二次曲线w的5个参数。w=（kkT)-1.通过cholesky分解可以求得内参。或者直接在联立方程的时候展开求得相机内参。DLT可以求得外参，之后用重投影误差(用到最大似然估计)来矫正。