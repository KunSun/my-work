# 安装 

aprilcal的作者给出了安装的[流程](https://april.eecs.umich.edu/software/java.html),根据该流程可以安装并使用。不过由于该软件很久没有维护，在版本和兼容问题上有一些需要注意的地方，下面逐步讨论。 

1.  安装用到的相关包。这里有很多 已经过期安装不了，像libglib2.0-dev，jdk等。这里要注意jdk版本尽量低，10 11 都不行。这里实在ubuntu 16上安装的jdk8 
2.  安装 lcm。最新版的lcm会不兼容，这里使用1.2.0版本。最好下载解压，不用 git clone
3.   设置环境变量 注意要在 home文件夹下打开.bashrc
4.   编译april可能会遇到一下问题。1.Base64类在april.util java.util中都有，要在出错的地方将Base64指定为april.util.Base64. 2. 找不到jni_md.h。这个是java的版本问题，我们在april/java/jni/commom.mk 在JNI_INCLUDES 后面添加  -I/usr/lib/jvm/java-8-openjdk-amd64/include -I/usr/lib/jvm/java-8-openjdk-amd64/include/linux 。[参考](https://stackoverflow.com/questions/37023234/fatal-error-jni-md-h-no-such-file-or-directory-include-jni-md-h)路径中的java版本号根据实际版本替换。至此就可以安装成功了。另外，在ubuntu 18上安装可能需要安装lua，最好选用5.2.4版本

# 使用  
1. 标定相机。打印指定的target作为标定板，在april.jar的路径下 打开终端，输入 java april.camera.calibrator.AprilCal -u "v4l2:///dev/video0" 即可开始标定(相机路径根据实际情况修改)。 标定完成后直接在aprilcal界面输入:save-calibration-image即可输出图片和相机参数。
2. 多目标定。
3. 图像矫正

# 原理
该软件采用交互式标定的方法，指定标定板需要放置的位置，易于操作且鲁棒性较高，精度也较好。该算法分为两个阶段，bootstrap和Max ERE算法流程如下：
## bootstrap 阶段 
由于用的是使用[Juho和 Sami提出的通用相机模型](http://www.ee.oulu.fi/mvg/files/pdf/pdf_697.pdf)，用3-4项泰勒展开来近似射影过程，矫正相机镜头畸变。 这是一种非线性模型，估计模型参数需要提供较好的初始值。其次标定相机模型至少需要三张图片。这就是本阶段存在的意义。
1. start camera 启动相机。这里初始化 candidate positions（数量在40~60之间）
2. initialize calibration.初始化标定。这里用简化的相机模型，只考虑焦距f，这样用single frame 就可以标定。标定方法就是张正友的方法，求解IAC。在启动相机后，就开始一直检测视野范围内的target，计算相机内参作为初始值。如果采样到某一帧图像标定出来的内参用在所有positions的误差低于一定阈值，则将该帧对应的标定结果替换为初始值。
3. suggesting first position。 对每一个pose计算frame score（内参误差评判）。对于每一个 candidate position，随机选取之前的n个frame(n>20)，计算内参误差。将内参误差越小分数越高，suggest best score position。（这里内参只有focal length，可以单幅图片标定）
4. 配准。根据GUI上的position提示，user拿着target板配准该位置。论文中未阐述判断是否对其的标准，猜测是MLS。
5. 用capture到的frame重新标定得到内参，然后推荐第二个position。通常caputer 3张frame。
6. 用这三张frame估计full camera model的参数（包括畸变 扭曲等10余个参数）。将其中的distortion 置零，用做下一阶段的输出。boostrap 阶段结束。


## 估计相机(full model)内参 
1. calibration and suggest next position 。这里使用Max ERE来计算内参误差。首先计算相机内外参的后验分布，然后计算内参的边缘分布（自变量的外参）。计算每个position的在控制点处的重投影误差，取最大值作为该position的误差(这就是Max的意思)。将误差最小的position推荐出来。
2. user 配准position。
3. 加入新的frame， recalibration。直到总共7~9张frame 全部收集完。标定完成。

# 代码
1. 该工程代码底层通信用c语言实现，make构建；算法实现用的java，ant构建。jni混合编译，只能在linux终端上运行，不太方便调试（java文件488个）。
2. porting的话，1.首先把项目中的java部分移植到IDE重新组织架构。这里不能解耦掉jni，调试起来比较麻烦。2. 将java部分重写为c++版本，工作量很大。或者打包成jar，再用c++调用。

## linux下的依赖
- [glib](https://gitlab.gnome.org/GNOME/glib/tree/glib-2-56)  可以在windows下[安装](https://blog.csdn.net/owe/article/details/1603744)，比较繁琐.
- [gtk](https://gitlab.gnome.org/GNOME/gtk) 可以在windows下[安装](https://www.gtk.org/download/windows.php)，有很多依赖库，比较繁琐。
- [subversion](https://subversion.apache.org/packages.html#windows) 可以安装到windows
- [ncurse](https://www.gnu.org/software/ncurses/) 有windows下的安装[1](https://stackoverflow.com/questions/138153/is-ncurses-available-for-windows)和[2](https://maryjaneinchain.github.io/2016/03/01/ncurses%E7%AC%94%E8%AE%B0%EF%BC%881%EF%BC%89%E2%80%94%E2%80%94ncurses%E5%BA%93%E7%9A%84%E4%BB%8B%E7%BB%8D%E4%B8%8E%E5%AE%89%E8%A3%85/)方法，不确定是否可行。 
- openjdk jre 等可以在windows下安装
- autopoint 是[gettext](https://www.gnu.org/software/gettext/)的一部分，可以在windows下[安装](https://www.gnu.org/software/gettext/) 
- [Mesa 3d](https://www.mesa3d.org/download.html)  可以在windows下[安装](https://fdossena.com/?p=mesa/build.frag),有预编译好的可执行文件
- [dc1394](https://github.com/astraw/dc1394/tree/master/libdc1394) 相机控制库，资料少，可能[移植不了](https://micro-manager.org/wiki/Dc1394)。
- [lcm](https://lcm-proj.github.io/) 可以在windows[安装](https://github.com/lcm-proj/lcm) 
- 
